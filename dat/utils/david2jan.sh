#!/bin/bash
for file in "$@"
do
    if [ -f $file ]
    then
        if [ -z ${mydir+x} ]; then mydir=$(mktemp -d); fi
        cp $file $mydir/$file;
        sed -f jan2david.sed $mydir/$file > $file;
    fi
done
if [ ! -z ${mydir+x} ]; then rm -r $mydir; fi
