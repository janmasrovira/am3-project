module AM3.Main (main) where

import AM3.Instance
import AM3.RandomInstance
import AM3.Scripts
import AM3.Solution
import AM3.Solver
import GRASP
import System.Environment
import System.FilePath
import Text.Megaparsec (ParseError)

main :: IO ()
main = do
  args <- getArgs
  case args of
    ["-g", seed, dat] -> runGrasp (read seed) False dat >>= putStr
    _ -> putStrLn "nothing to do"
