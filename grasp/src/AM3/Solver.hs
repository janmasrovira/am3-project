-- | This module implements convenience functions for solving 'Instance's using
-- 'grasp'

module AM3.Solver (
  runGrasp
                  ) where


import AM3.Solution
import AM3.Instance
import GRASP
import Text.Megaparsec (ParseError)
import Control.Monad.Identity
import Control.Monad.Random

-- | Runs the grasp on a file.
runGrasp :: Int -> Bool -> FilePath -> IO String
runGrasp seed printSol dat = do
  par <- fromFile dat :: IO (Either ParseError Instance)
  case par of
    (Left e) -> return (show e)
    (Right i) ->
      let
        gp :: MonadRandom m => GParams Solution Candidate m
        gp = gparams maxit maxCand alph i
        res = case flip evalRand (mkStdGen seed) $ flip evalRandT (mkStdGen seed) (grasp gp) of
          Nothing -> "no solution"
          Just (sol, cost) -> unlines (["cost: " ++ show cost]
                                       ++ ["solution: " ++ show (connections sol) | printSol])
        info = unlines ["seed = " ++ show seed
                       , "alpha = " ++ show alph
                       , "maxitr = " ++ show maxit
                       , "maxCand = " ++ show maxCand]
      in return (unlines [info, res])
  where
    alph = 0.5
    maxCand = Just 3000
    maxit = 10

