{-# LANGUAGE QuasiQuotes, FlexibleContexts #-}

module AM3.Regex (
  scanCost
  , scanTime
  , scanCplexTime
  , scanTestNum
  ) where

import Text.Regex.PCRE.Heavy
import Safe
import Control.Monad
import System.FilePath

f = readFile "/home/jan/Projects/am3-project/dat/runs/run4/test16.grasp"
f1 = readFile "/home/jan/Projects/am3-project/dat/runs/run4/test16.out"
d = readFile "/home/jan/Projects/am3-project/dat/runs/run3/test30.dat"

scanCost :: String -> Maybe Int
scanCost = scanReadOne [re|\s*cost: (\d+)|]

scanTime :: String -> Maybe Double
scanTime = scanReadOne [re|time: ([0-9]*\.?[0-9]*)|]

scanCplexTime :: String -> Maybe Double
scanCplexTime = scanReadOne [re|\s*Total \(root\+branch&cut\) =\s* ([0-9]*\.?[0-9]*)|]


scanReadOne :: Read c => Regex -> String -> Maybe c
scanReadOne re = (fmap read . headMay . snd <=< headMay) . scan re

scanTestNum :: FilePath -> Maybe Int
scanTestNum = scanReadOne [re|test(\d+)|] . takeBaseName
