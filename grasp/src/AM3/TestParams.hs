-- | Definitions of some parameters 'Params' for the random generator of 'Instance's.
--
-- See function 'randomInstance'.
module AM3.TestParams where

import AM3.RandomInstance
import AM3.Instance -- imported for the haddock link.
import Control.Monad.Random


-- | ('Params', seed).
type Concrete = (Params, Int)

micro :: Params
micro = Params {
  _oP = 0.5
  , _NC = (8, 12)
  , _kR = (5, 10)
  , _dR = (2, 6)
  , _pR = (2, 3)
  , _fR = (2, 2)
  , _cI = 5
  , _uP = 0.7
  }

small :: Params
small = Params {
  _oP = 0.9
  , _NC = (80,120)
  , _kR = (1200, 1800)
  , _dR = (25,60)
  , _pR = (5,7)
  , _fR = (100, 200)
  , _uP = 0.8
  , _cI = 10
  }

-- 10 secs
medium0 :: Concrete
medium0 = (medium, 0)

-- 134 secs
medium12 :: Concrete
medium12 = (medium, 12)

medium :: Params
medium = Params {
  _oP = 0.6
  , _NC = (550,550)
  , _kR = (80, 190)
  , _dR = (25,60)
  , _pR = (15,15)
  , _fR = (100, 200)
  , _uP = 0.8
  , _cI = 10
  }

-- 1550.66 secs
-- cost: 872616
big0 :: Concrete
big0 = (big, 0)

big :: Params
big = Params {
  _oP = 0.62
  , _NC = (700,700)
  , _kR = (180, 390)
  , _dR = (25,160)
  , _pR = (15,15)
  , _fR = (1000, 2000)
  , _uP = 0.8
  , _cI = 8
  }

big2 :: Params
big2 = Params {
  _oP = 0.60
  , _NC = (800,800)
  , _kR = (175, 380)
  , _dR = (25,160)
  , _pR = (15,25)
  , _fR = (100, 200)
  , _uP = 0.8
  , _cI = 5
  }

large :: Params
large = Params {
  _oP = 0.60
  , _NC = (900,1100)
  , _kR = (190, 390)
  , _dR = (25,160)
  , _pR = (15,15)
  , _fR = (100, 200)
  , _uP = 0.8
  , _cI = 10
  }
