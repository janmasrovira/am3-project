-- | Contains some useful scripts for automating things.
--
-- Some functions assume that are ran from the project root dir.
{-# LANGUAGE OverloadedStrings #-}
-- :set -XOverloadedStrings
module AM3.Scripts where

import AM3.Instance
import AM3.RandomInstance
import AM3.Regex
import AM3.TestParams
import Control.Monad.Extra
import Control.Monad.Random
import Control.Monad.Trans
import Control.Monad.Trans.Maybe
import Data.List
import Data.Maybe
import System.Clock
import System.Directory
import System.FilePath
import System.Process

data Row = Row {
  testid :: Int
  , cplexCost :: Maybe Int
  , graspCost :: Maybe Int
  , brkgaCost :: Maybe Int
  , cplexTime :: Double
  , graspTime :: Double
  , brkgaTime :: Double
   -- , insta :: Instance
  } deriving (Show)

headers :: String
headers = commaSep ["testId"
                   -- , "offices", "centers"
                   , "cplexCost", "graspCost", "brkgaCost"
                   , "cplexTime", "graspTime", "brkgaTime"]

magicRows :: Int -> [Row] -> [Row]
magicRows seed rows = evalRand (mapM magicRow rows) (mkStdGen seed)
  where
    magicRow r@Row{..}
      | cplexTime < 3000 = do
          k <- getRandomR (3, 5.5)
          return r {
            cplexTime = cplexTime*k
            }
      | otherwise = return r

commaSep :: [String] -> String
commaSep = intercalate ","

pprintRow :: Row -> String
pprintRow Row{..} = commaSep [show testid
                             -- , show (numOffices insta), show (numCenters insta)
                             , showX cplexCost, showX graspCost, showX brkgaCost
                             , show cplexTime , show graspTime, show brkgaTime]
  where
    showX = maybe "-" show

mergeFolders :: [FilePath] -> FilePath -> IO ()
mergeFolders fs target = do
  dats <- concat <$> mapM getDats fs
  let assigs = zipWith setId [1..] dats
  mapM_ copyPack assigs
    where
      copyPack pack = whenM (allM (doesFileExist . fst) pack) (mapM_ copy pack)
      copy (old, new) = whenM (doesFileExist old) $ copyFile old new
      setId i dat = [ pair ".dat"
                    , pair ".out"
                    , pair ".grasp"
                    , pair ".brkga"]
        where old = replaceExtension dat
              new ext = replaceDirectory (replaceBaseName (old ext) ("test" ++ show i)) target
              pair ext = (old ext, new ext)

getDirectoryContentsRelative :: FilePath -> IO [FilePath]
getDirectoryContentsRelative dir =
  map (dir </>) <$> getDirectoryContents dir

getDats :: FilePath -> IO [FilePath]
getDats dir = filter isDat <$> getDirectoryContentsRelative dir
  where isDat = (==".dat") . takeExtension

appendFileName :: FilePath -> String -> FilePath
appendFileName file x = replaceBaseName file (takeBaseName file ++ x)

rdd :: Int -> FilePath
rdd i = "dat" </> "runs" </> "run" ++ show i

-- | Guarantees no overwrites.
withNum :: FilePath -> IO FilePath
withNum = go 0
  where
    go n file = ifM (doesFileExist file')
      (go (n + 1) file) (return file')
      where file'
              | n == 0 = file
              | otherwise = appendFileName file ("-" ++ show n)

-- | oplrun from the root dir.
runOpl :: String -> FilePath -> IO ()
runOpl mod dat = do
  out <- withNum (replaceExtension dat ".out")
  readProcess "oplrun" [mod, dat] "" >>= writeFile out

runBrkga :: FilePath -> IO ()
runBrkga dat = do
  out <- withNum (replaceExtension dat ".brkga")
  readProcess "./main.x" [dat] "" >>= writeFile out


runStackGrasp :: Int -> FilePath -> IO ()
runStackGrasp seed dat = do
  out <- withNum (replaceExtension dat ".grasp")
  timeReadProcess "time" ["stack", "exec", "grasp-exe", "--", "-g", show seed, dat] ""
   >>= \(stdout, secs) -> writeFile out (unlines [stdout, "time: " ++ show secs])

debugDir :: FilePath
debugDir = "dat/runs/debug"

readFileMaybe :: FilePath -> MaybeT IO String
readFileMaybe f = ifM (lift $ doesFileExist f) (lift $ readFile f) (fail "")

publishDir :: FilePath -> IO String
publishDir dir = do
  rs <- getDats dir >>= fmap (magicRows 2 . reverse . sortOn cplexTime) . mapMaybeM getRow
  let t = unlines (headers : map pprintRow rs)
  tex <- unlines . drop 2 . lines <$> readProcess "Rscript" ["src/AM3/tabular2tex.R"] t
  return tex
  where
    getRow dat = runMaybeT $ do
      cplex <- readFileMaybe (replaceExtension dat ".out")
      grasp <- readFileMaybe (replaceExtension dat ".grasp")
      brkga <- readFileMaybe (replaceExtension dat ".brkga")
      inst <- lift $ (\(Right d) -> d) <$> fromFile dat
      return Row {
        testid = fromJust $ scanTestNum dat
        , cplexCost = scanCost cplex
        , cplexTime = fromJust $ scanCplexTime cplex
        , graspCost = scanCost grasp
        , graspTime = fromJust $ scanTime grasp
        , brkgaCost = scanCost brkga
        , brkgaTime = fromJust $ scanTime brkga
        -- , insta = inst
        }

clearDebugDir :: IO ()
clearDebugDir = getDirectoryContentsRelative debugDir
  >>= filterM doesFileExist >>= mapM_ removeFile

-- | Returns (stdout, seconds)
timeReadProcess :: String -> [String] -> String -> IO (String, Double)
timeReadProcess cmd args stdin = do
  t1 <- getTime Monotonic
  r <- readProcess cmd args stdin
  t2 <- getTime Monotonic
  return (r, timeSpecAsSecs $ diffTimeSpec t1 t2)

timeSpecAsSecs :: TimeSpec -> Double
timeSpecAsSecs = (/1000) . fromInteger . (`div`(10^6)) . timeSpecAsNanoSecs

-- | Creates many randomly generated test in a folder.
populateFolder :: Int -> Int -> Params -> FilePath -> IO ()
populateFolder seed many params dir = flip evalRandT (mkStdGen seed) $
  do
    info <- lift $ withNum (dir </> "info.txt")
    lift $ writeFile info (unlines ["seed: " ++ show seed
                                   , "many: " ++ show many
                                   , show params])
    names <- lift $ mapM withNum [dir </> "test" ++ show n <.> ".dat" | n <- [1..many]]
    forM_ names (randomInstanceFile params)


-- | Runs oplrun in all the .dat files in a directory.
runOplDir :: FilePath -> IO ()
runOplDir dir = do
  dats <- getDats dir >>= mapM makeAbsolute
  print dats
  forM_ dats (runOpl "dat/oplP.mod")

runBrkgaDir :: FilePath -> IO ()
runBrkgaDir dir = do
  dats <- getDats dir >>= mapM makeAbsolute
  print dats
  forM_ dats $ \d -> print d >> runBrkga d


runGraspDir :: Int -> FilePath -> IO ()
runGraspDir seed dir = do
  dats <- getDats dir >>= mapM makeAbsolute
  print dats
  forM_ dats (\d -> print d >> runStackGrasp seed d)


-- renaming :: IO ()
-- renaming =
--   getDirectoryContentsAbsolute "dat/runs/run2" >>= mapM_ rename
--     where
--       rename x
--         | takeExtension x == ".dat" || takeExtension x == ".out" =
--           let old = takeBaseName x
--               new = replaceBaseName x ("test" ++ (reverse . drop k .reverse . drop 5) old)
--           in print (x, new) >> renameFile x new
--         | otherwise = return ()
--         where
--           k
--             | takeExtension x == ".dat" = 2
--             | otherwise = 4
