#include <iostream>
#include <algorithm>
#include "brkgaAPI/BRKGA.h"
#include "brkgaAPI/MTRand.h"
#include "Decoder.hpp"
#include <chrono>
using namespace std;
using namespace std::chrono;

int main(int argc, char* argv[]) {
  if (argc != 2) {
    cout << "Usage: ./main.x datfile.dat" << endl;
    return 0;
  }
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();
  const unsigned p = 100;		// size of population
	const double pe = 0.10;		// fraction of population to be the elite-set
	const double pm = 0.10;		// fraction of population to be replaced by mutants
	const double rhoe = 0.70;	// probability that offspring inherit an allele from elite parent
	const unsigned K = 3;		// number of independent populations
	const unsigned MAXT = 5;	// number of threads for parallel decoding

  Decoder decoder;				// initialize the decoder
  string file = argv[1];
  decoder.readInstance(file);
  cout << "READ: " + file << endl;
  unsigned n = decoder.instance.nC;		// size of chromosomes
  printf("chromosome size: %u\n", n);


	const long unsigned rngSeed = 0;	// seed to the random number generator
	MTRand rng(rngSeed);				// initialize the random number generator

	// initialize the BRKGA-based heuristic
	BRKGA< Decoder, MTRand > algorithm(n, p, pe, pm, rhoe, decoder, rng, K, MAXT);

	unsigned generation = 0;		// current generation
	const unsigned X_INTVL = 100;	// exchange best individuals at every 100 generations
	const unsigned X_NUMBER = 2;	// exchange top 2 best
	const unsigned MAX_GENS = 30;
  cout << "Running for " << MAX_GENS << " generations..." << endl;
	do {
		algorithm.evolve();	// evolve the population for one generation

		if((++generation) % X_INTVL == 0) {
			algorithm.exchangeElite(X_NUMBER);	// exchange top individuals
		}
    cout << "generation " << generation << endl;
	} while (generation < MAX_GENS);

	// print the fitness of the top 10 individuals of each population:
  cout << "Fitness of the top 10 individuals of each population:" << endl;
	const unsigned bound = std::min(p, unsigned(10));	// makes sure we have 10 individuals
	for(unsigned i = 0; i < K; ++i) {
    cout << "Population #" << i << ":" << endl;
		for(unsigned j = 0; j < bound; ++j) {
      cout << "\t" << j << ") "
          << algorithm.getPopulation(i).getFitness(j) << endl;
		}
	}

  cout << "Best solution found has objective value = "
      << algorithm.getBestFitness() << endl;

  printf("cost: %i\n", int(algorithm.getBestFitness()));
  end = std::chrono::system_clock::now();
  std::chrono::duration<double> elapsed_seconds = end-start;
  std::cout << "time: " << elapsed_seconds.count() << "\n";

	return 0;
}
