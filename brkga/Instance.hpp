#ifndef INSTANCE_H
#define INSTANCE_H

#include <vector>
#include <iostream>
#include <string>
using namespace std;

class Instance {
 public:
  Instance();

  void readFile(string filePath);
  double eval(const vector<double>& chromosome) const;
  double eval2(const vector<double>& chromosome) const;


  int nO;
  int nC;
  int nP;
  int r;

  vector<int> d;
  vector< vector<int> > u;
  vector<int> k;
  vector<int> f;
  vector<int> s;
  vector<int> m;
private:
  int at(const vector<double>& chr, int c, int o) const;
  int scaleX(double x, int c, int o) const;
  int price(int c, int d) const;
  void putData(const double chr, int& con, int& cleft, int& dleft, int& tleft, int c, int o) const;
  static int scale(double x, int a, int b);
  static const double pen;
  static const double incr;
};

#endif
