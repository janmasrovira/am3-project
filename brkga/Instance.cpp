#include "Instance.hpp"
#include <fstream>
#include <algorithm>
#include <chrono>
using namespace std;

const double Instance::pen = 1e5;
const double Instance::incr = 0.1;

Instance::Instance() {
  r = 2;
  nO = nC = nP = 0;
}

int readInt(ifstream& in, bool sep=false) {
  int r;
  if (sep) {
    char c;
    in >> c >> r >> c;
  }
  else
    in >> r;
  return r;
}

// reads "[1 2 3]"
// when sep = true, also reads "=" and ";"
vector<int> readIntList(vector<int>& v, int n, ifstream& in, bool sep=false) {
  char c;
  v = vector<int> (n);
  if (sep) in >> c;
  in >> c; // [
  for (int i = 0; i < n; ++i) {
    in >> v[i];
  }
  in >> c; // ]
  if (sep) in >> c;
  return v;
}

void readIntList2(vector< vector<int> >& v, int n, int m, ifstream& in, bool sep=false) {
  v = vector< vector<int> > (n);
  char c;
  if (sep) in >> c;
  in >> c;
  for (int i = 0; i < n; ++i) {
    readIntList(v[i], m, in);
  }
  in >> c;
  if (sep) in >> c;
}


// price due to center c if it had d units of data stored.
int Instance::price(int c, int d) const {
  if (d == 0) return 0;
  int p = f[c];

  // finding the segment
  int is = -1;
  while (is + 1 < nP and m[is + 1] <= d) ++is;
  if (is < 0) return pen;
  p += s[is]*d;
  return p;
}

// puts as much data as possible.
void Instance::putData(double chr, int& con
                       , int& cleft, int& dleft
                       , int& tleft, int c, int o) const {
  if (chr < 0.2) chr = 0;
  int kchr = chr * k[c]; // limited center capacity.
  int much = min(min(kchr, cleft) , min(d[o], dleft));
  cleft -= much;
  dleft -= much;
  tleft -= much;
  con += much;
}

// One chromosome per center.
// It limits the capacity of the center.
double Instance::eval(const vector<double> &chr) const {
  vector<double> z = chr;

  // connections.
  vector< vector<int> > con (nC, vector<int> (nO, 0));

  // space left per center
  vector<int> cleft(nC);
  for (int i = 0; i < nC; ++i) cleft[i] = k[i];

  // data left per office
  vector<int> dleft(nO);
  int tleft = 0; // data left in all offices.
  for (int i = 0; i < nO; ++i) {
    tleft += r*d[i];
    dleft[i] = r*d[i];
  }

  // random shuffle of Centers
  vector<int> cix(nC);
  for (int i = 0; i < nC; ++i) cix[i] = i;
  // random_shuffle(cix.begin(), cix.end());

  //random shuffle of offices
  vector<int> oix(nO);
  for (int i = 0; i < nO; ++i) oix[i] = i;
  random_shuffle(oix.begin(), oix.end());

  double tincr = 0;
  while (tleft > 0 and tincr <= 1.0) {
    for (int ixc = 0; ixc < nC; ++ixc)
      for (int ixo = 0; ixo < nO; ++ixo) {
        int c = cix[ixc];
        int o = oix[ixo];
        if (1 == u[c][o] and con[c][o] == 0)
          putData(z[c], con[c][o], cleft[c], dleft[o], tleft, c, o);
        else {}
          //printf("(%i, %i) uco %i, con %i\n", c, o, u[c][o], con[c][o]);
      }
    // not enough space --> grow the chromosome.
    if (tleft > 0) {
      tincr += incr;
      for (int i = 0; i < nC; ++i) {
        z[i] = min(1.0, z[i] + incr);
      }
    }
  }

  if (tleft > 0) return 1e12;

  double cost = 0;
  //cost calculation
  for (int c = 0; c < nC; ++c) {
    int pr = price(c, k[c] - cleft[c]);
    // printf("price c kc cleft pr %i %i %i, %i\n", c, k[c], cleft[c], pr);
    cost += pr;
  }
  return cost;
}


void Instance::readFile(string filePath) {
  ifstream in(filePath.c_str());
  string str;
  while (in >> str) {
    cout << "reading " << str << endl;
    if (str == "NO") {
      nO = readInt(in, true);
    }
    else if (str == "bigM") {
      readInt(in, true);
    }
    else if (str == "NC") {
      nC = readInt(in, true);
    }
    else if (str == "NP") {
      nP = readInt(in, true);
    }
    else if (str == "r") {
      r = readInt(in, true);
    }
    else if (str == "d") {
      readIntList(d, nO, in, true);
    }
    else if (str == "k") {
      readIntList(k, nC, in, true);
    }
    else if (str == "f") {
      readIntList(f, nC, in, true);
    }
    else if (str == "s") {
      readIntList(s, nP, in, true);
    }
    else if (str == "m") {
      readIntList(m, nP, in, true);
    }
    else if (str == "u") {
      readIntList2(u, nC, nO, in, true);
    }
    else {
      cout << "unrecognized identifier " << str << endl;
    }
  }
}
