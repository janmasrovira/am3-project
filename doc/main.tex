\documentclass[paper=a4, fontsize=11pt, abstract=on, parskip=half*]{scrartcl} 
% \documentclass[a4paper]{report}
\usepackage{lmodern}

% Use 8-bit encoding that has 256 glyphs
% \usepackage[T1]{fontenc} % for lualatex
\usepackage[utf8]{inputenc} % for pdflatex

% Math packages
\usepackage{amsfonts,amsthm,amsmath}

% Language/hyphenation
\usepackage[english]{babel}
% \selectlanguage{catalan} 
\usepackage{authblk}

\usepackage{csquotes}
\usepackage{textcomp}

% \usepackage{enumitem}
\usepackage{nameref}
\usepackage{mathabx}
\usepackage{framed}

\usepackage{lipsum}
\usepackage{graphicx}
\usepackage{adjustbox}
\usepackage{longtable}
\usepackage{subcaption}
\usepackage{nth}
\usepackage{booktabs}
% checkmark
\usepackage{pifont}

\usepackage{hyperref}
\usepackage{minted}
\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
    citecolor=Green3
}

\usepackage[
backend=biber,
style=ieee,
sortlocale=de_DE,
natbib=true,
url=false, 
doi=true,
eprint=false
]{biblatex}
\addbibresource{biblio.bib}
% \bibliography{biblio}

% inline lists
\usepackage{paralist}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage[x11names]{xcolor}

% Allo table recolocation
\usepackage{float}
\restylefloat{table}

\usepackage{setspace}
\setstretch{1.2}

\begin{document}
% \renewcommand{\baselinestretch}{1.2}
\newcommand{\horrule}[1]{\rule{\linewidth}{#1}} % Create horizontal

\begin{titlepage}
  \begin{center}


    % \textsc{\LARGE Facultat d'informàtica de Barcelona, UPC}\\[1.5cm]
    \begin{figure}[H]
      \centering
    \end{figure}
    \textsc{\Large Algorithmic Methods for Mathematical Models}\\[0.5cm]

    % Title
    \horrule{0.5pt} \\[0.4cm]
    { \huge \bfseries Applied Linear Programming and Metaheuristics \\[0.4cm] }
    % \Large Lliurable final\\

    \horrule{2pt} \\[2.4cm]

    % Author and supervisor
    \noindent
    \begin{minipage}{0.4\textwidth}
      \begin{flushleft} \large
        \emph{Author:}\\
        Jan Mas Rovira
      \end{flushleft}
    \end{minipage}%
    \begin{minipage}{0.4\textwidth}
      \begin{flushright} \large
        \emph{Teachers:} \\
        Albert Oliveras LLunell \\
        Luis Domingo Velasco Esteban
      \end{flushright}
    \end{minipage}


    \vfill
    
    % Bottom of the page
    {\large \today}

  \end{center}
\end{titlepage}

\tableofcontents
\newpage

\section{Introduction}
\label{sec:intro}
Combinatorial problems consist in finding an optimum solution from a finite set
of possible solutions. These problems are extremely time-consuming because it
does not exist any efficient algorithm to solve them. For small to medium sized
instances finding an optimal solution is feasible using, for instance, a linear
solver. However, as the input grows, a point will be reached where an exhaustive
solver will not end in a reasonable time. Thus, we need to look for a better
approach. Algorithms based on metaheuristics are specially designed to deal with
the bigger instances with limited time. On the other hand, usually they will not
return the optimal solution.

In this project we engage a difficult optimization problem from three different
angles:

\begin{enumerate}[(a)]
\item \textit{Linear programming} We formalize the problem as a set of linear
  constraints and we encode them using OPL. We use Cplex-Ilog as the solver.
\item \textit{biased random-key genetic algorithms (BRKGA)} With the help of a
  library\cite{toso2015c++} we only need to implement a specific decoder for our
  problem in order to have a solver based on a genetic algorithm.
\item \textit{Greedy randomized adaptive search procedure (GRASP)} We implement
  a GRASP based on \cite{resende2009greedy} in Haskell to solve our problem. 
\end{enumerate}

For the GRASP part, we also implemented an interface similar to the one offered
by \cite{toso2015c++} but in Haskell. We have put a lot of effort into fully
documenting it so we invite you to read it its documentation. The library can be found
\href{https://hackage.haskell.org/package/grasp-0.1.0.0}{here}. We will give
more details of it as we go along, specially in section \ref{sec:grasp}.

In section \ref{sec:replic} we give instructions on how to replicate all the experiments.

All the code, documentation and tests from this project are freely available on
my \href{https://bitbucket.org/janmasrovira/am3-project}{bitbucket page}. 


\section{Problem statement}
A large company produces huge amounts of data and wants to store them as monthly
backups. The company has $O$ different regional offices and we know, for each office
$o$, the amount of data $d_o$ in Peta Bytes (1 PB = $10^6$ GB) that it needs to store.
The company has not the necessary resources to store such amount of data, so it
decides to hire external backup providers.

Due to latency constraints, every office can use only some of the backup centers.
Given an office $o$ and a center $c$, this information it is specified by parameter $u_{co}$,
equal to 1 if such a connection is allowed.
For security reasons, every backed data needs to be stored in two different backup
centers.

After studying several possibilities, several backup providers are chosen, resulting
in $C$ different backup centers, where every backup center $c$ can store at most $k_c$ PBs
of data.

Using a backup center $c$ has a fixed cost $f_c$ and an additional cost for every PB it
stores. The cost of storing data will be computed in $P$ segments of decreasing cost
that are the same for all backup centers. For every cost segment, $s_p$ specifies the cost
per PB stored and $m_p$ the minimum stored data to apply cost segment $p$.

\textit{Example}: Let us assume three cost segments, where storing less than 7 PBs costs
5,000 EUR per PB, less than 12 PBs costs 3,000 EUR each, and storing 12 PBs
or more costs 2,000 EUR each. Then, storing 14 PBs will cost 14*2,000 EUR.

We assume that the above parameters for cost and data are integers and that only
integer amounts of PBs from every office can be stored.In this project, we have
been asked to elaborate a plan for the company. We have to find out which backup
centers will be used and how many PBs from each office every backup center will
store so that the total cost is minimized.

\section{Integer linear model}
% \label{sec:orgheadline5}
The model in OPL is \href{./files/oplP.mod}{here}.

\subsection{Input variables}
\label{sec:orgheadline1}
\begin{itemize}
\item The company has a set \(O\) of offices.
\item A set \(C\) of centers are available.
\item Each office needs to store \(d_o\) units of data.
\item \(u_{co}\) tells if a center \(c\) can be connected to office \(o\).
\item \(k_c\) tells how much center \(c\) can store.
\item Using center \(c\) has fixed cost \(f_c\) plus an additional cost. The
  additional cost is specified by segments.
\item \(s_p\) specifies the additional cost of segment \(p\), which requires a minimum
  of \(m_p\) units of storage to be applied.
\item Data needs to be replicated \(r\) times\footnote{This a generalization I
    made of the problem statement. I considered that it would be adequate to
    abstract the number of replications since it does not increase the
    complexity of the model.}. Since we need two replications, we set \(r=2\).
  Let \(B = \{1,2, \dots, r\}\) the set of backup identifiers.
\end{itemize}

\subsection{Main Decision variables}
\label{sec:orgheadline2}
\begin{itemize}
\item \(x_{co} \in \mathbb{N}\) tells how much data from office \(o\) is stored
  in center \(c\).
\end{itemize}
\subsection{Auxiliary decision variables}
\label{sec:orgheadline3}
\begin{itemize}

\item \(a_{cp} \in \mathbb{N}\) is the amount of data stored in center \(c\) using
  segment $p$.
  \begin{equation}
    \forall c \in C: \sum_p a_{cp} = \sum_o x_{cop}
  \end{equation}

\item \(t_c \in \mathbb{B}\) is 1 if center \(c\) is used.
  \begin{equation}
    \label{eq:center_used}
    \forall c \in C : t_c k_c \geq \sum_o x_{co}
  \end{equation}

  \begin{equation}
    \label{eq:one_segment}
    \forall c \in C, p \in P:  t_c k_c \geq a_{cp} 
  \end{equation}
\item \(z_{cp} \in \mathbb{B}\) is 1 if segment \(p\) is applied to center
  \(c\). See constraints \ref{orgtarget1} and \ref{orgtarget2}.

\item \(w_c \in \mathbb{N}\) is the total cost of center \(c\).
  \begin{equation}
    \forall c \in C : w_c = f_c t_c + \sum_p a_{cp} s_p
  \end{equation}
\item \(v\in \mathbb{N}\) is the total cost that we want to minimize.
\end{itemize}
\begin{equation}
  v = \sum_c w_c
\end{equation}
\subsection{Constraints}
\label{sec:orgheadline4}
\begin{enumerate}
\item \label{orgtarget3} All the data in an office must be stored.
  \begin{equation}
    \forall o \in O : \sum_c \sum_p x_{cop} = r\cdot d_o
  \end{equation}

\item \label{orgtarget4} Every backed data needs to be stored in \(r\) different
  centers. It suffices to ensure that a center does not contain more than $d_o$
  data from from the same office.
  \begin{equation}
    \forall c \in C, o \in O :  \sum_p x_{cop} \leq d_o
  \end{equation}

\item \label{orgtarget5} A data center cannot exceed its capacity. Already
  guaranteed by equation \ref{eq:center_used}.

\item \label{orgtarget6} Only allowed connections may be used.
  \begin{equation}
    \forall c \in C, o \in O : u_{co}k_c \geq \sum_p x_{cop}
  \end{equation}

\item \label{orgtarget2} A center is assigned exactly one segment if it is used. 
  \begin{equation}
    \forall c \in C : \sum_p z_{cp} = t_c
  \end{equation}

\item \label{orgtarget1} The segment \(s_p\) can only be used if at least
  \(m_p\) data units are stored in that center.
  \begin{equation}
    \forall c \in C, p \in P : a_{cp} \geq m_p z_{cp}
  \end{equation}
\end{enumerate}

\section{BRKGA}

As we mentioned in the introduction, we will use a library that only requires us
to implement the decoder.

\subsection{Decoder}
The duty of the decoder is to construct a valid solution from a set of
chromosomes and evaluate its cost. A chromosome is commonly implemented using
bits, integers or doubles. The library that we use opted to use doubles in
the interval ${0,1}$.

Our decoder uses one chromosome for each center. We multiply the capacity of
each center by its associated chromosome. Then we iterate over the potential
connections ($offices \bigtimes centers$) in a random order assigning at each
iteration as much data as the constraints allow us to. 
 
Finally, we compute the cost of the constructed solution and we return it. If
the decoder was not able to assign all the data we return a very large cost as a
penalization.

\section{GRASP}
In this section we explain how we solved the problem using a GRASP.

\subsection{Our library's interface}
\label{sec:grasp}
Since implementing an interface of
a GRASP requires the ability to pass functions as parameters, Haskell was the
perfect fit for our goal.

As in the \texttt{C++} BRKGA API case, our library offers an interface to a
generic implementation of a GRASP algorithm. The homepage of the library can be found
\href{./files/doc/grasp-0.1.0.0/index.html}{here}. In the BRKGA case, we only needed to
implement the \textit{decoder}. In the GRASP case, due to its nature, several
functions must be implemented specially for each problem so the list of
functions that the user must implement is longer. Here is the full list:

\begin{enumerate}
\item \textbf{Empty solution} constructor.
\item A \textbf{generator of candidates}.
\item A \textbf{greedy function} that estimates the cost of each candidate.
\item A function that appends a candidate to the solution.
\item A function that \textbf{verifies} if a solution is valid. If a solution has no
  candidates and is invalid is discarded and we skip the iteration.
\item A \textbf{neighborhood} generator.
\item A \textbf{cost function} for a valid solution.
\end{enumerate}

Additionally, the following parameters can be given:

\begin{itemize}
\item \textbf{$\alpha \in [0,1]$}: A parameter that is used in the definition of
  the Restricted Candidate List (RCL). It is a balance between greediness and
  randomness. By default its value is set to 0.5.
\item \textbf{Maximum iterations}: The number of iterations that the algorithm
  will perform. It will return the best generated solution.
\end{itemize}

\subsubsection{Potential improvement}
The main loop of the GRASP algorithm does not have any dependency on the
previous iterations so it is an ideal algorithm to parallelize. Since it is not
the goal of the subject i decided to keep the library simple and not parallelize
it. Nonetheless, this optimization would result in great speedups.

\subsection{The library applied to our case}
\label{sec:graspimp}

Since most functions required by our library are trivial, here we will only
explain the implementation of the candidate generator, greedy function and the
neighborhood generator.

\paragraph{Candidate generator}
After testing several approaches we have come to the conclusion that what is
simple works best.
For each center $c$ and office $o$, if they are allowed to be connected and they
are not already connected, we put as much data as possible from $o$ in $c$. The
amount of data is limited by the space that it is left on the center and the
amount of data that the office has not stored yet. Remember that for each office
we must store a total of $2d_o$ units of data because it must be replicated, however,
a connection to a center cannot exceed $d_o$ units of data, otherwise, the data may not
be replicated in different centers.

\paragraph{Greedy function} Our greedy function is simply defined as the
solution cost increase that is associated to the candidate. Let $f$ be the cost
function. Then the greedy function is defined as $g(sol, cand) = f(sol \cup cand)
- f(sol)$. Even though it is defined like this, its implementation is optimized
so it only recalculates the cost associated with the center of the candidate.

With $g$ being the greedy function described above, the definition of the
restricted candidate list is
\begin{equation*}
RCL = \{ x \in Cand \ |\ g(x) \leq x^{min} + \alpha (x^{max} - x^{min})\}
\end{equation*}
% \begin{algorithm}
%   \caption{Candidate generator algorithm}\label{candidateGen}
%   \begin{algorithmic}[1]
%     \Procedure{candidates}{$solution$}
%     \State $Cand\gets \emptyset$
%     \State $A \gets allowedConnections(solution)$ \Comment{Every potential connection} 
%     \ForAll{$o \in Offices, c \in Centers$} 
%     \If{$spaceLeft(c) > 0 \textbf{ and } dataLeft(o) > 0 \textbf{ and } (c, o)
%       \in A$}
    
%     \State{$Cand \gets Cand \cup \{(c, o, d)\}$} \Comment{Add the connection to
%       the candidates}
%     \EndIf
%     \EndFor\label{euclidendwhile}
%     \State \textbf{return} $Cand$
%     \EndProcedure
%   \end{algorithmic}
% \end{algorithm}

\paragraph{Neighborhood}
Once again, we follow a quite simple approach. For each existing connection from
office $o$ in center $c1$ for $d$ units of data, we move different amounts of
data to $c2$. The amount is limited by the constraints of the problem and the
number of different amounts is bounded by a small constant.

\section{Experiments}
\subsection{Random instance generator}
In this section we will explain how we generated the random instances for the
experiments. Except for the segment costs, it is very straightforward.

The generator is part of our library so you can find its documentation and
source code \href{./doc/grasp-0.1.0.0/AM3-RandomInstance.html}{here}.

The generation process is parameterized with the variables that are described
below.
\begin{enumerate}
\item We say that an entity is either an office or a center. The number of
  entities is picked uniformly at random (u.a.r.) from a range $rn$.
\item Each entity has a probability $p_o$ of becoming an office and $1-p_o$ of
  becoming a center. % Note that $\mathbf{E}(\frac{numOffices}{numCenters}) =
                     % p_o$.
\item The capacity of each center is picked u.a.r. from a range $rk$.
\item The data of each office is picked u.a.r. from a range $rd$.
\item The number of segments is picked u.a.r. from a range $rp$.
\item The length $l_i$ of each segment is picked u.a.r. from the range $[1\dots
  \frac{\mathbf{E}(k)}{P}]$, where $P$ is the number of segments and
  $\mathbf{E}(k)$ is the expected capacity of a center (the mid point of the
  range $rk$). We impose that the threshold (minimum amount of data required) of
  the first segment is $m_o = 0$. For the other thresholds $m_i =
  m_{i-1}+l_{i-1}$.
\item Each fixed center cost is picked u.a.r. from a range $rf$.
\item Each potential connection is allowed with a constant probability $p_u$.
\item Finally the cost of each segment is calculated. We decided to add the
  constraint to the input that the cost associated with any center must
  monotonically increase with respect to the stored data. In other words, we
  cannot get a smaller cost by adding data to a center.

  Remember that $m_i$ is the threshold of segment $i$ (the minimum amount of data
  required).\\
  Let $c_i$ be the cost of segment $i$.
  
  Then it must hold that
  $$m_ic_i > (m_i - 1)c_{i-1}$$
  Which is the same as
  $$ c_i > \frac{(m_i - 1)c_{i-1}}{m_i}$$
  Then we define $c_i$ as
  \begin{align*} \label{eq1}
      c_0 &= I_0 \\
      c_i &= I_i + \frac{(m_i - 1)c_{i-1}}{m_i} && \text{$i > 0$}
  \end{align*}
  Where $I_i$ is a positive random integer.
\end{enumerate}

\subsection{Results}
\label{sec:results}

In this section we compare the timings and quality of the solutions that each
one of the methods obtained.

Below there is a table with the results of the tests that took most time to run
on Cplex, in annex \ref{sec:full} there is a full table. The input of each test is available \href{./files/dat}{here} and in
section \ref{sec:replic} we explain how to replicate the results.
\begin{longtable}{r|rrr|rrr}
  \toprule
testId & cplexCost & graspCost & brkgaCost & cplexTime & graspTime & brkgaTime \\ 
  \midrule
 27 & 405381 & 405636 & 4187734 & 20797.64 & 567.39 & 153.87 \\ 
   12 & 223168 & 223636 & 3009028 & 3447.19 & 631.07 & 103.14 \\ 
    3 & 45348 & 45940 & 3879348 & 3851.09 & 751.40 & 107.08 \\ 
   44 & 146369 & 146411 & 1803905 & 2567.37 & 210.12 & 22.36 \\ 
   13 & 47341 & 47534 & 4311824 & 1852.43 & 612.95 & 234.36 \\ 
   20 & 209246 & 211217 & 3429125 & 667.91 & 840.55 & 117.14 \\ 
   39 & 60243 & 60243 & 1457035 & 618.94 & 205.69 & 29.18 \\ 
   85 & 307435 & 308210 & 1961479 & 398.86 & 269.29 & 12.84 \\ 
   30 & 131614 & 132715 & 3911599 & 560.38 & 865.94 & 83.73 \\ 
   80 & 281983 & 283570 & 2561919 & 302.78 & 285.40 & 9.82 \\ 
   38 & 54004 & 55148 & 1482320 & 381.19 & 231.59 & 19.86 \\ 
   82 & 24711 & 26292 & 1384393 & 331.99 & 191.39 & 6.96 \\ 
   19 & 43848 & 45301 & 2621513 & 270.53 & 673.44 & 113.97 \\ 
   71 & 112215 & 113331 & 1644153 & 323.09 & 233.54 & 10.04 \\ 
   41 & 58359 & 58684 & 1154308 & 210.74 & 202.01 & 11.39 \\ 
   \bottomrule
\end{longtable}

The results show that for some tests the running time of the linear programming
approach make it infeasible for real world use. The GRASP algorithm is the clear
winner for the most difficult results. The solution it gives exceed a maximum of
a $2\%$ of the opimal one and the execution times are much more stable. On the
other hand, the BRKGA algorithm performs well in terms of execution time but
their solutions tend to be poor.


\subsection{Replication}
\label{sec:replic}
In every project that presents experimental results it is crucial to provide
detailed instructions on how to rerun the experiments. In this section we will
give a concise explanation of how to do it.

\subsubsection{Requirements}
\textcolor{red}{Important note}: I ran all the executions on my laptop using Ubuntu 14.04. It
should work on any linux distribution, however, it do not know if it will work on Windows.
\begin{enumerate}[(a)]
\item You need the \textbf{Cplex} optimization suite and the program \texttt{oplrun} must
  be on the path of your computer.
\item You need a tool called \texttt{stack}, which is a builder for Haskell
  projects. It can be easily installed following
  \href{http://docs.haskellstack.org/en/stable/install_and_upgrade.html}{these instructions}.
\end{enumerate}

\subsubsection{Steps}

\begin{enumerate}
\item Clone the project to your computer.
\begin{minted}{text}
git clone git@bitbucket.org:janmasrovira/am3-project.git ~/am3-project
\end{minted}
\item Compile the BRKGA algorithm.

\mintinline{text}{cd brkga && make}
\item Setup \texttt{stack} and build the GRASP library. This library also contains
  all the scripts needed to automatize the execution of experiments. This step may
  take a while.

\mintinline{text}{cd grasp && stack setup && stack build}
\item It is all ready now. In order to run all the \texttt{.dat} files in a
  directory with each one of the three methods, you just have to type

\mintinline{text}{stack exec grasp-exec -- -a dir/runs/run6}

For each \texttt{.dat} file three files containing the results of each method
will be created in the same directory.


\end{enumerate}

\printbibliography{}

\section{Annex: All results}
\label{sec:full}
All the timings and costs of all the experiments sorted by cplexTime.

\begin{longtable}{r|rrr|rrr}
  \toprule
testId & cplexCost & graspCost & brkgaCost & cplexTime & graspTime & brkgaTime \\ 
  \midrule
  \endhead
 27 & 405381 & 405636 & 4187734 & 20797.64 & 567.39 & 153.87 \\ 
   12 & 223168 & 223636 & 3009028 & 3447.19 & 631.07 & 103.14 \\ 
    3 & 45348 & 45940 & 3879348 & 3851.09 & 751.40 & 107.08 \\ 
   44 & 146369 & 146411 & 1803905 & 2567.37 & 210.12 & 22.36 \\ 
   13 & 47341 & 47534 & 4311824 & 1852.43 & 612.95 & 234.36 \\ 
   20 & 209246 & 211217 & 3429125 & 667.91 & 840.55 & 117.14 \\ 
   39 & 60243 & 60243 & 1457035 & 618.94 & 205.69 & 29.18 \\ 
   85 & 307435 & 308210 & 1961479 & 398.86 & 269.29 & 12.84 \\ 
   30 & 131614 & 132715 & 3911599 & 560.38 & 865.94 & 83.73 \\ 
   80 & 281983 & 283570 & 2561919 & 302.78 & 285.40 & 9.82 \\ 
   38 & 54004 & 55148 & 1482320 & 381.19 & 231.59 & 19.86 \\ 
   82 & 24711 & 26292 & 1384393 & 331.99 & 191.39 & 6.96 \\ 
   19 & 43848 & 45301 & 2621513 & 270.53 & 673.44 & 113.97 \\ 
   71 & 112215 & 113331 & 1644153 & 323.09 & 233.54 & 10.04 \\ 
   41 & 58359 & 58684 & 1154308 & 210.74 & 202.01 & 11.39 \\ 
   47 & 204576 & 205414 & 1550234 & 165.62 & 256.84 & 9.63 \\ 
    8 & 296541 & 298971 & 4319417 & 162.92 & 1120.69 & 103.24 \\ 
   76 & 238727 & 241044 & 1893901 & 245.09 & 323.97 & 11.28 \\ 
   79 & 106177 & 107682 & 1884290 & 159.66 & 296.68 & 7.34 \\ 
   43 & 226224 & 227281 & 1661430 & 186.26 & 245.74 & 9.92 \\ 
   55 & 219164 & 221120 & 1393981 & 74.89 & 317.64 & 8.00 \\ 
   49 & 283408 & 284678 & 1456404 & 44.92 & 285.46 & 9.63 \\ 
   59 & 111239 & 112421 & 1668493 & 68.72 & 265.56 & 10.09 \\ 
   66 & 218842 & 220818 & 1893081 & 31.94 & 318.29 & 9.44 \\ 
   69 & 111479 & 112533 & 1427154 & 49.03 & 230.93 & 9.60 \\ 
   10 & - & - & - & 43.59 & 338.14 & 355.65 \\ 
   14 & - & - & - & 38.16 & 351.76 & 372.71 \\ 
   15 & - & - & - & 38.60 & 324.23 & 364.17 \\ 
    9 & - & - & - & 24.76 & 307.55 & 346.11 \\ 
   31 & - & - & - & 37.79 & 314.94 & 244.13 \\ 
   25 & - & - & - & 33.78 & 285.96 & 211.63 \\ 
   11 & - & - & - & 25.06 & 343.43 & 362.77 \\ 
   17 & - & - & - & 37.11 & 317.64 & 338.00 \\ 
   24 & 311573 & 312497 & 4039088 & 36.14 & 749.25 & 78.95 \\ 
   23 & - & - & - & 30.41 & 316.96 & 237.71 \\ 
    7 & - & - & - & 36.71 & 302.18 & 336.80 \\ 
    5 & - & - & - & 23.45 & 315.59 & 330.67 \\ 
   22 & - & - & - & 24.27 & 325.51 & 225.62 \\ 
   18 & - & - & - & 34.29 & 324.23 & 355.31 \\ 
   26 & - & - & - & 19.92 & 304.27 & 246.64 \\ 
   84 & 138203 & 139517 & 1610928 & 21.75 & 236.98 & 12.63 \\ 
    6 & - & - & - & 20.05 & 311.24 & 341.40 \\ 
   29 & - & - & - & 27.26 & 328.20 & 265.50 \\ 
   16 & 220832 & 221990 & 3604359 & 20.74 & 1027.34 & 107.27 \\ 
   77 & 26474 & 27630 & 1708052 & 25.29 & 251.97 & 10.34 \\ 
    2 & - & - & - & 27.35 & 349.42 & 323.94 \\ 
   51 & 220578 & 222314 & 1700351 & 18.28 & 305.69 & 9.60 \\ 
   21 & - & - & - & 16.32 & 333.42 & 343.71 \\ 
    4 & 384236 & 386207 & 4422879 & 15.98 & 1079.73 & 98.63 \\ 
    1 & 206240 & 209092 & 2978616 & 22.74 & 1176.21 & 78.00 \\ 
   28 & 470389 & 471264 & 5350033 & 21.93 & 780.68 & 72.17 \\ 
   87 & 161990 & 163984 & 1931058 & 19.27 & 235.90 & 10.79 \\ 
   52 & 297178 & 299514 & 1874372 & 19.79 & 402.12 & 9.25 \\ 
   40 & 137532 & 139365 & 1735993 & 14.40 & 305.98 & 8.36 \\ 
   78 & 84666 & 85869 & 1874629 & 13.72 & 247.46 & 11.39 \\ 
   70 & 201599 & 202483 & 2176202 & 11.40 & 221.79 & 9.75 \\ 
   65 & 52253 & 53935 & 1038635 & 16.24 & 221.06 & 7.25 \\ 
   74 & - & - & - & 8.70 & 123.48 & 31.34 \\ 
   56 & 155166 & 157962 & 1579039 & 11.24 & 318.13 & 6.80 \\ 
   60 & - & - & - & 10.55 & 127.29 & 32.19 \\ 
   57 & - & - & - & 14.73 & 123.45 & 31.84 \\ 
   81 & - & - & - & 12.52 & 127.76 & 32.08 \\ 
   48 & - & - & - & 11.88 & 127.67 & 31.26 \\ 
   83 & - & - & - & 12.48 & 119.63 & 31.97 \\ 
   58 & - & - & - & 12.64 & 127.42 & 32.45 \\ 
   42 & - & - & - & 11.52 & 117.47 & 31.97 \\ 
   73 & - & - & - & 10.92 & 127.88 & 31.37 \\ 
   75 & - & - & - & 11.68 & 121.27 & 40.42 \\ 
   54 & - & - & - & 12.88 & 118.27 & 31.47 \\ 
   62 & - & - & - & 10.08 & 123.51 & 31.39 \\ 
   72 & 109396 & 110601 & 2103974 & 9.19 & 212.98 & 9.39 \\ 
   68 & - & - & - & 12.49 & 122.25 & 32.15 \\ 
   45 & 304761 & 306722 & 1991829 & 10.29 & 416.70 & 8.79 \\ 
   50 & - & - & - & 7.65 & 120.89 & 31.15 \\ 
   64 & - & - & - & 10.38 & 123.07 & 30.28 \\ 
   86 & 53507 & 54764 & 1586924 & 7.88 & 235.49 & 11.55 \\ 
   67 & - & - & - & 9.61 & 114.51 & 30.17 \\ 
   46 & 221244 & 222560 & 2048454 & 8.35 & 261.97 & 9.53 \\ 
   61 & 134444 & 136179 & 1677240 & 8.01 & 243.88 & 7.18 \\ 
   63 & 238714 & 240915 & 1910767 & 7.64 & 369.19 & 8.13 \\ 
   53 & 113362 & 114233 & 2175145 & 9.49 & 230.36 & 9.73 \\ 
   \bottomrule
\end{longtable}

\end{document}