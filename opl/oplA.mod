/*********************************************
 * OPL 12.6.0.0 Model
 * Author: jan
 * Creation Date: Jan 1, 2016 at 8:08:36 PM
 *********************************************/
   
 
int bigM = ...;
 
int NO = ...; // num offices
int NC = ...; // num centers
int NP = ...; // num segments
int r = ...; // replications (usually 2)
 
range O = 1..NO;
range C = 1..NC;
range P = 1..NP;
range B = 1..r;

int d[o in O] = ...; // needed storage
int u[c in C][o in O] = ...; // allowed connections
int k[c in C] = ...; // center capacities
int f[c in C] = ...; // fixed costs
int s[p in P] = ...; // segments
int m[p in P] = ...; // segment min storage


// Decision
dvar int x[c in C][o in O][b in B][p in P]; // data assignment

// Aux
dvar boolean y[c in C][o in O][b in B]; // bool connection usage
dvar int a[c in C][p in P]; // data in centers
dvar boolean t[c in C]; // bool center usage
dvar boolean z[c in C][p in P]; // segment assignment
dvar int w[c in C]; // per center cost
dvar int v; // total cost


 minimize v;
 
 subject to {
 
 	// (0) x \in Naturals
 	forall (c in C, o in O, b in B, p in P) {
 		x[c][o][b][p] >= 0; 	 	
 	}
  	
  	// (1)
  	forall(c in C, o in O, b in B)
  	  y[c][o][b]*bigM >= sum(p in P) x[c][o][b][p];
  	  
  	// (2)
  	forall(c in C, p in P)
  	  a[c][p] == sum(o in O, b in B) x[c][o][b][p];
  	  
  	// (3)
  	forall(c in C)
  	  t[c]*bigM >= sum(p in P) a[c][p];
  	  
  	// (4)  
  	forall(c in C)
  	  w[c] == f[c]*t[c] + sum(p in P) a[c][p]*s[p];
  	  
	// (5)
	v == sum(c in C) w[c];
	
	// (6)
	forall(o in O, b in B) 
		sum(c in C, p in P) x[c][o][b][p] == d[o];
  	
  	// (7)
  	forall(c in C, o in O)
  	  sum(b in B) y[c][o][b] <= 1;
  	  
  	// (8)
  	forall(c in C)
  	  sum(o in O, b in B, p in P) x[c][o][b][p] <= k[c];
  	  
	// (9)
	forall(c in C, o in O)
	  u[c][o]*bigM >= sum(b in B, p in B) x[c][o][b][p];
	  
	// (10)
	forall(c in C)
	  sum(p in P) z[c][p] == t[c];
	  
	// (11)
	forall(c in C, p in P)
	  a[c][p] >= m[p]*z[c][p];

}
   
 execute {
   for (var c in C) for (var o in O) {
   		for (var b in B) for (var p in P) {
   			var xx = x[c][o][b][p];	   		
   		
   			if (xx > 0) {
   				writeln("Use connection between center " + c + " and office " + o 
   				+ " for " + xx + " units of data.");		
   			}   		
   		}
       
   }
  
 }