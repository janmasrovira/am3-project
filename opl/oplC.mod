

int NO = ...; // num offices
int NC = ...; // num centers
int NP = ...; // num segments
int r = ...; // replications (usually 2)

range O = 1..NO;
range C = 1..NC;
range P = 1..NP;

int d[o in O] = ...; // needed storage
int u[c in C][o in O] = ...; // allowed connections
int k[c in C] = ...; // center capacities
int f[c in C] = ...; // fixed costs
int s[p in P] = ...; // segments
int m[p in P] = ...; // segment min storage


// Decision
dvar int+ x[c in C][o in O]; // data assignment

// Aux
dvar int+ a[c in C][p in P]; // data in centers
dvar boolean t[c in C]; // bool center usage
dvar boolean z[c in C][p in P]; // segment assignment
dvar int w[c in C]; // per center cost
dvar int v; // total cost

 minimize v;

 subject to {

 	// (1) segment assignation
	forall(c in C)
  		sum(o in O) x[c][o] == sum(p in P) a[c][p];

  	// (2)
  	forall(c in C)
  	  t[c]*k[c] >= sum(o in O) x[c][o];

  	// (3) only one segment
	forall (c in C, p in P)
	  a[c][p] <= t[c]*k[c];

  	// (4)
  	forall(c in C)
  	  w[c] == f[c]*t[c] + sum(p in P) a[c][p]*s[p];

	// (5)
	v == sum(c in C) w[c];

	// (6)
	forall(o in O)
		sum(c in C) x[c][o] == r * d[o];

  	// (7)
  	forall(c in C, o in O)
  	  x[c][o] <= d[o];


	// (8)
	forall(c in C, o in O)
	  u[c][o]*k[c] >= x[c][o];

	// (9)
	forall(c in C)
	  sum(p in P) z[c][p] == t[c];

	// (10)
	forall(c in C, p in P)
	  a[c][p] >= m[p]*z[c][p];

}

 execute {
	writeln("MODEL B");
// 	for (var c in C) for (var o in O) {
//   		for (var p in P) {
//   			var xx = x[c][o][p];
//
//   			if (xx > 0) {
//   				writeln("Use connection between center " + c + " and office " + o
//   				+ " for " + xx + " units of data.");
//   			}
//   		}
//
//   }
   writeln("cost = " + v);

 }

main {
  cplex.threads = 1;
  thisOplModel.generate();
  if (cplex.solve()) {
          thisOplModel.printSolution();
          writeln("cost: " + thisOplModel.v);
     }
     else {
          writeln("no solution");
     }
  }
